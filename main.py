# Python Object Oriented Programming adventure game
import cmd

class EdgeOfMapException(Exception):
    pass
class PathBlockedException(Exception):
    pass

class Room:
    def __init__(self, items=[], doors=[True]*4, intro=''):
        self.items = items
        self.doors = doors
        self.intro = intro
    def is_open(self, direction):
        if self.doors[direction]:
            return True
        else:
            raise PathBlockedException
    def enter(self):
        print(self.intro)

crash_site = Room(items=['scrapmetal'], intro='''The mothership has crashed, you are on a strange planet and everyone else is probably dead. :)
    You should probably try and find a way to contact home. The wreckage is blocking all directions except for a small path to the East''', doors=[False, True, False, False])
big_field = Room(intro='''You find youself in a large expanse of land, there are many directions to travel in.''', doors=[True, True, True, True])
dense_forest = Room(intro='''You are surrounded by thick blue trees. You can see a clearing North and East.''', doors=[True, True, False, True])
massive_hole = Room(intro='''There is simply a big hole in the floor, will you jump into it? There is a path to the West''', doors=[False, False, False, True])
korok_den = Room(items=['rock'], intro='''There is a rock on the floor, it looks out of place? There are paths to the North and East''', doors=[True, True, False, False])
nothing = Room(items=['nothing'], intro='''There is literally nothing here. There are paths to the East and South''', doors=[False, True, True, False])
end = Room(intro='''You find an intergalactic phone box perhaps you should phone home on it. There is a path to the West''', doors=[False, False, False, True])
something = Room(items=['something'], intro='''There is something here maybe you need nothing to replace it with There is a path to the West''', doors = [False, False, False, True])
river = Room(intro='''There is a soothing green river here, is that an interglactic phone box you see to the East?! There are paths to the East, South and West''', doors=[False, True, True, True])

map = [[nothing, river, end],
    [korok_den, big_field, something],
    [crash_site, dense_forest, massive_hole]]

class Player:
    inventory = []
    turns = 0
    location = [0, 0]  # x, y
    current_room = crash_site

    def update_room(self):
        self.current_room = map[2-self.location[1]][self.location[0]]
        self.current_room.enter()

class GameEngine(cmd.Cmd):
    intro = 'type "help" if you are stuck'
    prompt = '> '
    player = Player()
    player.current_room.enter()

    def do_jump(self, args):
        '''you can jump down or over things with this command'''
        if self.player.current_room == massive_hole:
            print('You jumped down the hole and die. What were you expecting?')
            return True
        else:
            print('You aren\'t able to jump anywhere here')

    def do_info(self, args):
        '''gives some basic info about the room you are in'''
        print(self.player.current_room.intro)

    def do_call(self, args):
        '''you can do this to call home'''
        if self.player.current_room != end:
            print('What are you going to call home on?')
        elif '5 space quid' in self.player.inventory:
            print('You insert the 5 space quid and call home, they will arrive shortly.\nWell done for beating the game!')
            return True
        else:
            print('You need to insert 5 space quid to call home and your wallet blew up in the explosion of the mothership')

    def do_phone(self, args):
        return self.do_call(args)
            
    def do_take(self, item):
        '''you can take an item from the area you are in, example: take scrapmetal'''
        if item in self.player.current_room.items:
            if item == 'rock':
                print('A strange nature spirit appears from underneath the rock, shouting "YAHAJA YOU FOUND ME". You drop the rock back onto its head in surprise. "OW"')
            elif item == 'something':
                if 'nothing' in self.player.inventory:
                    print('You replace the mysterious object with nothing and realise it is 5 space quid')
                    self.player.inventory.append('5 space quid')
                    self.player.current_room.items.pop(self.player.current_room.items.index(item))
                    self.player.inventory.pop(self.player.inventory.index('nothing'))
                    self.player.current_room.intro = 'There is literally nothing here except the nothing you left'
                    self.player.current_room.enter()
            else:
                self.player.inventory.append(item)
                self.player.current_room.items.pop(self.player.current_room.items.index(item))
                print('You pocket the ' + item.lower())
        else:
            print('there is no ' + item + ' in this area.')

    def do_go(self, direction):
        '''you can use this to travel throughout the game, example: go west'''
        try:
            if direction in ['up', 'north']:
                if self.player.location[1] == 2:
                    raise EdgeOfMapException
                self.player.current_room.is_open(0)
                self.player.location[1] += 1
            elif direction in ['right', 'east']:
                if self.player.location[0] == 2:
                    raise EdgeOfMapException
                self.player.current_room.is_open(1)
                self.player.location[0] += 1
            elif direction in ['down', 'south']:
                if self.player.location[1] == 0:
                    raise EdgeOfMapException
                self.player.current_room.is_open(2)
                self.player.location[1] -= 1
            elif direction in ['left', 'west']:
                if self.player.location[0] == 0:
                    raise EdgeOfMapException
                self.player.current_room.is_open(3)
                self.player.location[0] -= 1
            else:
                print('I can\'t understand where you want to go')
            self.player.update_room()
        except PathBlockedException:
            print('Your path is blocked in that direction')
        except EdgeOfMapException:
            print('You\'re at the edge of the map!')
    
    def do_quit(self, confirm=''):
        '''what do you think it does'''
        if confirm == 'confirm':
            return True
        else:
            print('Please confirm by typing: `quit confirm`')

GameEngine().cmdloop()
